package com.example.robolectric;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.R;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboActivity {
    public static final String SHARED = "sharedUserData";

    public static final String DATA_EMAIL = "DATA_EMAIL";
    public static final String DATA_PASSWORD = "DATA_PASSWORD";

    @InjectView(R.id.email)
    EditText email;

    @InjectView(R.id.password)
    EditText password;

    @InjectView(android.R.id.button1)
    Button login;

    private SharedPreferences mSharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLoginData();
            }
        });

        mSharedPrefs = getSharedPreferences(SHARED, Context.MODE_PRIVATE);

    }

    private void getLoginData() {
        login(email.getText().toString(),password.getText().toString());
    }

    public void login(String email, String password) {
        //TODO

        saveUserData(email, password);
        goToHomeActivity();
    }

    public void saveUserData(String email, String password) {
        mSharedPrefs
                .edit()
                .putString(DATA_EMAIL, email)
                .putString(DATA_PASSWORD, password)
                .apply();

    }

    public void goToHomeActivity() {
        Intent home = new Intent(this, HomeActivity.class);
        startActivity(home);
    }

    /**
     *
     * @return true if the user data was set
     */
    public boolean checkForLoginData() {
        return (mSharedPrefs.contains(DATA_EMAIL) && mSharedPrefs.contains(DATA_PASSWORD));
    }

    public void autologin() {

    }

}
