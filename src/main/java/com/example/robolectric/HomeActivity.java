package com.example.robolectric;

import android.os.Bundle;
import android.widget.TextView;
import com.example.R;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * deckard-gradle-master
 * <p/>
 * Created by david on 29.11.14.
 */
@ContentView(R.layout.activity_home)
public class HomeActivity extends RoboActivity {
    @InjectView(android.R.id.text1)
    TextView welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setWelcomeMessage(String name) {
        welcome.setText(getString(R.string.welcome_name, name));
    }
}
