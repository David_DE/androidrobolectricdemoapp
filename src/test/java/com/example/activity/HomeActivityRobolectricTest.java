package com.example.activity;

import android.widget.TextView;
import com.example.robolectric.HomeActivity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * deckard-gradle-master
 * <p/>
 * Created by david on 29.11.14.
 */
@Config(manifest = "./src/main/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class HomeActivityRobolectricTest {
    HomeActivity activity;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.buildActivity(HomeActivity.class).create().visible().get();
    }

    @Test
    public void testSetup() throws Exception {
        assertTrue(activity != null);
    }

    @Test
    public void testWelcomeMsg() {
        activity.setWelcomeMessage("TestName");

        TextView welcomeTv = (TextView) activity.findViewById(android.R.id.text1);
        assertNotNull("TextView should not be null", welcomeTv);
        assertEquals("Welcome message is not correct!", "Welcome TestName!", welcomeTv.getText().toString());
    }

    @Config(qualifiers = "de")
    @Test
    public void testWelcomeMsgGerman() {
        activity.setWelcomeMessage("TestNameGerman");

        TextView welcomeTv = (TextView) activity.findViewById(android.R.id.text1);

        assertNotNull("TextView should not be null", welcomeTv);
        assertEquals("Welcome message is not correct!", "Willkommen TestNameGerman!", welcomeTv.getText().toString());
    }
}
