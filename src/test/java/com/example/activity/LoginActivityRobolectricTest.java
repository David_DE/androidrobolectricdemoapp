package com.example.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.TextView;
import com.example.R;
import com.example.robolectric.HomeActivity;
import com.example.robolectric.LoginActivity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.*;

@Config(manifest = "./src/main/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class LoginActivityRobolectricTest {
    LoginActivity activity;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.buildActivity(LoginActivity.class).create().visible().get();
    }

    @Test
    public void testSomething() throws Exception {
        assertTrue(activity != null);
    }

    @Test
    public void testText() {
        TextView tv = (TextView) activity.findViewById(R.id.text);

        assertNotNull("The TextView is null!", tv);

        assertEquals("The message is not correct", "Login to System", tv.getText());
    }

    @Test
    public void testLaunchingHomeActivity() {
        activity.goToHomeActivity();

        Intent expectedIntent = new Intent(activity, HomeActivity.class);

        ShadowActivity shadowActivity = Robolectric.shadowOf(activity);

        assertEquals(
                "This should be identicall!",
                expectedIntent.getComponent().getClassName(),
                shadowActivity.getNextStartedActivity().getComponent().getClassName()
        );

    }

    @Test
    public void testSavingUserDataToShares() {
        //test that the correct data gets collected

        String email = "blub@mail.com";
        String password = "12345678";
        activity.saveUserData(email, password);

        SharedPreferences sharedPreferences = activity.getSharedPreferences(LoginActivity.SHARED, Context.MODE_PRIVATE);

        assertEquals("Email was not saved", sharedPreferences.getString(LoginActivity.DATA_EMAIL, ""), email);
        assertEquals("Password was not saved", sharedPreferences.getString(LoginActivity.DATA_PASSWORD, ""), password);

    }

    @Test
    public void testCheckForLoginData() {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(LoginActivity.SHARED, Context.MODE_PRIVATE);

        sharedPreferences.edit().remove(LoginActivity.DATA_EMAIL).remove(LoginActivity.DATA_PASSWORD).apply();
        assertFalse(activity.checkForLoginData());

        sharedPreferences.edit().putString(LoginActivity.DATA_EMAIL, "blub").putString(LoginActivity.DATA_PASSWORD, "123").apply();
        assertTrue(activity.checkForLoginData());

        sharedPreferences.edit().remove(LoginActivity.DATA_EMAIL).remove(LoginActivity.DATA_PASSWORD).apply();
        assertFalse(activity.checkForLoginData());
    }

    @Test
    public void testSendingHttpLogin() {

    }

    @Test
    public void testParsingLoginResponse() {

    }

    @Test
    public void testLoginErrorResponse() {
        //test what happens when the entered user data is invalid
    }

    @Test
    public void testLoginNoConnectionNoResponse() {

    }

}
